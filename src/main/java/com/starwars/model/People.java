package com.starwars.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class People {

    @JsonProperty("name")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}