package com.starwars.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public abstract class AbstractController {

    @Autowired
    protected RestTemplate restTemplate;
    private static ObjectMapper mapper;

    public ObjectMapper getObjectMapperInstance(){
        if (mapper == null) {
            mapper = new ObjectMapper();
        }
        return mapper;
    }
}