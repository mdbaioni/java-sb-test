package com.starwars.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.starwars.model.People;

@RestController
public class StarWarsController extends AbstractController {

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	@ResponseBody
	public List<People> testMethod() throws JsonProcessingException {

		HttpEntity<String> entity = getHttpEntity();

		ObjectMapper mapper = this.getObjectMapperInstance();

		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

		ResponseEntity<String> response = restTemplate.exchange("https://swapi.co/api/people", HttpMethod.GET, entity,
				String.class);

		System.out.println(response.getBody());

		JsonObject jsonObject = new Gson().fromJson(response.getBody(), JsonObject.class);

		List<People> people = new ArrayList<People>();

		try {
			people = mapper.readValue(jsonObject.get("results").toString(),
					mapper.getTypeFactory().constructCollectionType(List.class, People.class));
			people.stream().forEach(p -> System.out.println(p));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return people;

	}

	private HttpEntity<String> getHttpEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		return entity;
	}
	
}
